package cn.itechyou.cms.controller.tools;

import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.hutool.json.JSONUtil;
import cn.itechyou.cms.common.ResponseResult;
import cn.itechyou.cms.common.StateCodeEnum;
import cn.itechyou.cms.utils.StringUtil;

/**
 * Json工具类
 * @author Jonas
 *
 */
@RestController
@RequestMapping("tools/json")
public class JsonToolController {
	
	/**
	 * 格式化Json
	 * @param params
	 * @return
	 */
	@PostMapping("format")
	public ResponseResult format(@RequestBody Map<String,String> params) {
		ResponseResult result = null;
		if(!params.containsKey("json") || StringUtil.isBlank(params.get("json"))) {
			result = ResponseResult.Factory.newInstance(Boolean.FALSE, StateCodeEnum.HTTP_SUCCESS.getCode(), null, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		}
		try {
			JSONObject jsonObject = JSONObject.parseObject(params.get("json"));
			String jsonString = JSONObject.toJSONString(jsonObject,true);
			result = ResponseResult.Factory.newInstance(Boolean.TRUE, StateCodeEnum.HTTP_SUCCESS.getCode(), jsonString, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		} catch (Exception e) {
			result = ResponseResult.Factory.newInstance(Boolean.FALSE, StateCodeEnum.HTTP_SUCCESS.getCode(), null, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		}
	}
	
	/**
	 * 压缩Json
	 * @param params
	 * @return
	 */
	@PostMapping("compress")
	public ResponseResult compress(@RequestBody Map<String,String> params) {
		ResponseResult result = null;
		if(!params.containsKey("json") || StringUtil.isBlank(params.get("json"))) {
			result = ResponseResult.Factory.newInstance(Boolean.FALSE, StateCodeEnum.HTTP_SUCCESS.getCode(), null, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		}
		try {
			JSONObject jsonObject = JSONObject.parseObject(params.get("json"));
			String jsonString = JSONObject.toJSONString(jsonObject);
			result = ResponseResult.Factory.newInstance(Boolean.TRUE, StateCodeEnum.HTTP_SUCCESS.getCode(), jsonString, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		} catch (Exception e) {
			result = ResponseResult.Factory.newInstance(Boolean.FALSE, StateCodeEnum.HTTP_SUCCESS.getCode(), null, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		}
	}
	
	/**
	 * 转义Json
	 * @param params
	 * @return
	 */
	@PostMapping("transform")
	public ResponseResult transform(@RequestBody Map<String,String> params) {
		ResponseResult result = null;
		if(!params.containsKey("json") || StringUtil.isBlank(params.get("json"))) {
			result = ResponseResult.Factory.newInstance(Boolean.FALSE, StateCodeEnum.HTTP_SUCCESS.getCode(), null, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		}
		try {
			String quote = JSONUtil.quote(params.get("json"),false);
			result = ResponseResult.Factory.newInstance(Boolean.TRUE, StateCodeEnum.HTTP_SUCCESS.getCode(), quote, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		} catch (Exception e) {
			result = ResponseResult.Factory.newInstance(Boolean.FALSE, StateCodeEnum.HTTP_SUCCESS.getCode(), null, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		}
	}
	
	/**
	 * 去掉转义
	 * @param params
	 * @return
	 */
	@PostMapping("deTransform")
	public ResponseResult deTransform(@RequestBody Map<String,String> params) {
		ResponseResult result = null;
		if(!params.containsKey("json") || StringUtil.isBlank(params.get("json"))) {
			result = ResponseResult.Factory.newInstance(Boolean.FALSE, StateCodeEnum.HTTP_SUCCESS.getCode(), null, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		}
		try {
			String quote = StringEscapeUtils.unescapeJava(params.get("json"));
			result = ResponseResult.Factory.newInstance(Boolean.TRUE, StateCodeEnum.HTTP_SUCCESS.getCode(), quote, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		} catch (Exception e) {
			result = ResponseResult.Factory.newInstance(Boolean.FALSE, StateCodeEnum.HTTP_SUCCESS.getCode(), null, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		}
	}
}
