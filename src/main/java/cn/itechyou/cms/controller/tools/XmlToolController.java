package cn.itechyou.cms.controller.tools;

import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.druid.sql.SQLUtils;

import cn.itechyou.cms.common.ResponseResult;
import cn.itechyou.cms.common.StateCodeEnum;
import cn.itechyou.cms.utils.StringUtil;
import cn.itechyou.cms.utils.XmlUtils;

/**
 * Html工具类
 * @author Jonas
 *
 */
@RestController
@RequestMapping("tools/xml")
public class XmlToolController {
	
	/**
	 * 格式化Json
	 * @param params
	 * @return
	 */
	@PostMapping("format")
	public ResponseResult format(@RequestBody Map<String,String> params) {
		ResponseResult result = null;
		if(!params.containsKey("xml") || StringUtil.isBlank(params.get("xml"))) {
			result = ResponseResult.Factory.newInstance(Boolean.FALSE, StateCodeEnum.HTTP_SUCCESS.getCode(), null, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		}
		try {
			String string = XmlUtils.format(params.get("xml"));
			result = ResponseResult.Factory.newInstance(Boolean.TRUE, StateCodeEnum.HTTP_SUCCESS.getCode(), string, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		} catch (Exception e) {
			result = ResponseResult.Factory.newInstance(Boolean.FALSE, StateCodeEnum.HTTP_SUCCESS.getCode(), null, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		}
	}
	
}
